class UserAuthenticator
  def self.run(params)
    user = User.where(email: params[:email]).first
    user&.authenticate(params[:password])
  end
end
