class TransactionCreator

  def self.run(user, params)
    new(user, params).run
  end

  def initialize(user, params)
    @user, @params = user, params
  end

  def run
    @transaction = Transaction.create(@params.merge(user: @user))
    errors.empty?
  end

  def errors
    @transaction.errors.messages
  end
end