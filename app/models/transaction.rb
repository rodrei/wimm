# == Schema Information
#
# Table name: transactions
#
#  id           :bigint           not null, primary key
#  description  :string
#  amount_cents :integer
#  currency     :string
#  date         :datetime
#  user_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Transaction < ApplicationRecord
  CURRENCIES = %w{ USD ARS EUR }.freeze
  belongs_to :user

  validates :amount, presence: true
  validates :date,   presence: true

  def amount
    amount_cents && (amount_cents / 100)
  end

  def amount=(val)
    self.amount_cents = val * 100
  end
end
