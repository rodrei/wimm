# == Schema Information
#
# Table name: users
#
#  id              :bigint           not null, primary key
#  email           :string
#  password_digest :string
#  api_token       :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class User < ApplicationRecord
  has_secure_password

  before_create :set_api_token

  private

  def set_api_token
    self.api_token = SecureRandom.hex(Rails.env.production? ? 64 : 6)
  end
end
