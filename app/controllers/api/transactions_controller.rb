module Api
  class TransactionsController < BaseController
    def create
      service = TransactionCreator.new(current_user, transaction_params)
      if service.run
        head :created
      else
        render json: { errors: service.errors }, status: :unprocessable_entity
      end
    end

    private

    def transaction_params
      params.require(:transaction).permit(
        :description, :amount, :currency, :date
      )
    end
  end
end