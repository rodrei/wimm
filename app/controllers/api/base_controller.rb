module Api
  class BaseController < ApplicationController
    before_action :authenticate_user!

    def authenticate_user!
      if current_user.blank?
        head :unauthorized
      end
    end
  end
end
