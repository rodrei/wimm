module Api
  class SessionsController < BaseController
    skip_before_action :authenticate_user!

    def create
      @user = UserAuthenticator.run(session_params)
      if @user
        render json: { session: @user.attributes.slice('email', 'api_token') },
               status: :created
      else
        head :unprocessable_entity
      end
    end

    private

    def session_params
      params.require(:session).permit(:email, :password)
    end
  end
end
