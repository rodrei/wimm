class SessionsController < ApplicationController
  before_action :redirect_if_signed_in, except: :destroy

  def new
  end

  def create
    @user = UserAuthenticator.run(session_params)
    if @user
      sign_in(@user)
      redirect_to root_path
    else
      flash[:error] = 'Your email and password combination is incorrect'
      @session = OpenStruct.new(session_params)
      render :new
    end
  end

  def destroy
    sign_out
    redirect_to transactions_path
  end

  private

  def session_params
    params.require(:session).permit(:email, :password)
  end

  def redirect_if_signed_in
    redirect_to transactions_path if current_user.present?
  end
end
