require 'test_helper'

class TransactionCreatorTest < ActiveSupport::TestCase
  let(:user) { create(:user) }
  context 'given valid params' do
    let(:params) do
      {
        description: 'takeout',
        amount: "21,33",
        currency: "ARS",
        date: DateTime.now
      }
    end

    it 'creates a transaction and returns true' do
      assert TransactionCreator.run(user, params)
      assert_equal 1, Transaction.count
    end

    it 'returns an empty errors object' do
      service = TransactionCreator.new(user, params)
      service.run
      assert service.errors.empty?
    end
  end

  context 'given invalid params' do
    let(:params) do
      { description: 'takeout' }
    end

    it "doesn't create a transaction and returns false" do
      refute TransactionCreator.run(user, params)
      assert_equal 0, Transaction.count
    end

    it 'returns an errors object' do
      service = TransactionCreator.new(user, params)
      service.run
      assert service.errors.present?
      assert service.errors[:amount].present?
    end
  end

end