module IntegrationHelper
  def response_json
    if response.body.present?
      JSON.parse(response.body).deep_symbolize_keys!
    else
      {}
    end
  end

  def sign_in(user)
    post session_path, params: {
      session: { email: user.email, password: user.password }
    }
  end
end
