ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require 'minitest/focus'
require 'minitest/pride'
require 'helpers/integration_helper'
Dir[File.join(__dir__, 'factories', '*.rb')].each { |file| require file }

#require 'mocha/minitest'
#require 'capybara/rails'

class ActiveSupport::TestCase
  include FactoryBot::Syntax::Methods

  class << self
    alias_method :context, :describe
  end
end

class ActionDispatch::IntegrationTest
  include IntegrationHelper
end
