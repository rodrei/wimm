require 'test_helper'

class TransactionsTest < ActionDispatch::IntegrationTest
  context "given the user is not logged in" do
    it 'returns a 401 Unauthorized' do
      post '/api/transactions'
      assert_response :unauthorized
    end
  end

  context "given valid params" do
    before { sign_in(create(:user)) }

    let(:params) do
      {
        transaction: {
          description: 'takeout',
          amount: "21,33",
          currency: "ARS",
          date: DateTime.now
        }
      }
    end

    it "returns 201 created" do
      post '/api/transactions', params: params
      assert_response :created
    end

    it "creates a transaction" do
      post '/api/transactions', params: params
      assert_equal Transaction.count, 1
    end
  end

  context "given invalid params" do
    before { sign_in(create(:user)) }

    let(:params) do
      { transaction: { description: 'takeout' } }
    end

    it "returns 422 Unprocessable Entity" do
      post '/api/transactions', params: params
      assert_response :unprocessable_entity
    end

    it "returns an errors object" do
      post '/api/transactions', params: params
      assert response_json[:errors].present?
    end
  end
end