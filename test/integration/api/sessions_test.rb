require 'test_helper'

class SessionsTest < ActionDispatch::IntegrationTest
  describe "given valid credentials" do
    before do
      @user = create(:user, email: 'r@r.com', password: '12345678')
    end

    let(:params) do
      { email: 'r@r.com', password: '12345678' }
    end

    context "given valid params" do
      it "returns 200 ok" do
        post "/api/session", params: { session: params }
        assert_response :created
      end

      it "returns a user object" do
        post "/api/session", params: { session: params }
        assert_equal response_json[:session][:email], @user.email
        assert_equal response_json[:session][:api_token],  @user.api_token
      end
    end

    context 'given invalid params' do
      let(:params) do
        { email: 'whatever@r.com', password: '12345678' }
      end

      it "returns 411 unprocessable entity" do
        post "/api/session", params: { session: params }
        assert_response :unprocessable_entity
      end
    end
  end
end
