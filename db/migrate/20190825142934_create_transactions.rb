class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.string :description
      t.integer :amount_cents
      t.string :currency
      t.datetime :date
      t.integer :user_id
      t.timestamps
    end

    add_index :transactions, [:user_id, :date]
  end
end
