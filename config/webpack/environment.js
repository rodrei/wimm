const { environment } = require('@rails/webpacker')
const vue =  require('./loaders/vue')

environment.loaders.append('vue', vue)
environment.loaders.append('sass', {
  test: /\.sass$/,
  use: [
    'vue-style-loader',
    'css-loader',
    'sass-loader',
  ]
});
module.exports = environment
