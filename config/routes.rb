Rails.application.routes.draw do
  get 'transactions/index'
  get 'transactions/create'
  get 'transactions/destroy'
  resources :transactions

  namespace :api do
    resource :session, only: [:create]
    resources :transactions, only: [:create]
  end

  resource :session, only: [:create, :destroy]
  get '/login', to: 'sessions#new'


  root to: 'sessions#new'
end
